#pragma once


//setup an enumeration so we can refer to the textures by name rather than by number
static enum TextureNames
		{
			NO_TEXTURE, //0 is never used for texture mapping - OpenGL just won't assign this as a name for a texture binding
			MENU,
			SKYBOX,
			CRATE,
			FENCE,
			ENEMY,
			HUT,
			GRASS,
			EXPLOSION,
			WALK,
			FACE
		};

class MyGame :
	public CGame
{
public:
	MyGame(void);
	~MyGame(void);
	void Initialize();
};


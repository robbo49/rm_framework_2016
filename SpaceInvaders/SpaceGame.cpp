#include "stdafx.h"
#include "SpaceGame.h"
#include "PlayerBehavior.h"
#include "FireMissileBehavior.h"

CSpaceGame::CSpaceGame(void)
{
	Initialize();
}


CSpaceGame::~CSpaceGame(void)
{
}

void
CSpaceGame::Initialize()
{
		//load the textures
        CTextureLoader::getInstance().CreateTexture("assets/invadersbg.png", BACKGROUND);
		CTextureLoader::getInstance().CreateTexture("assets/alien1.tga", ALIEN);
		CTextureLoader::getInstance().CreateTexture("assets/mbase.tga", PLAYER);
        //add some objects
 
		//set orthographic rendering instead of perspective rendering mode
		CGraphics::b2DMode=true;

		//setup the camera
		m_pRootNode = new CGameObject();
		m_pRootNode->GetTransform()->SetPosition(0, 0, 5);
		m_pRootNode->SetRenderable(new CCamera());

		//setup the player game object
		CGameObject * pBG=new CGameObject();
        pBG->GetTransform()->SetPosition(0,0,0);
        pBG->GetTransform()->SetSize(7,5,1);
        pBG->SetRenderable(new CSprite());
        pBG->SetMaterial(new CColourMaterial());
        pBG->GetMaterial()->SetTexture(BACKGROUND);
        m_pRootNode->AddChild(pBG);

		//setup some aliens
		for (int i=0; i<1; i++)
		{
        CGameObject * pAlien=new CGameObject();
        pAlien->GetTransform()->SetPosition(-1.5+0.5*(float)i,1,1);
        pAlien->GetTransform()->SetSize(0.5,0.5,1);
        pAlien->SetRenderable(new CSprite());
        pAlien->SetMaterial(new CColourMaterial());
        pAlien->GetMaterial()->SetTexture(ALIEN);
        m_pRootNode->AddChild(pAlien);
		}
 
		CGameObject * PBase=new CGameObject();
        PBase->GetTransform()->SetPosition(0,-1,1);
        PBase->GetTransform()->SetSize(0.3,0.3,1);
        PBase->SetRenderable(new CSprite());
        PBase->SetMaterial(new CColourMaterial());
        PBase->GetMaterial()->SetTexture(PLAYER);
        m_pRootNode->AddChild(PBase);
		//PBase->AddBehavior(new CPlayerBehavior());
		//PBase->AddBehavior(new CFireMissileBehavior());

		/*CGameObject * pLMissile=new CGameObject();
        pLMissile->GetTransform()->SetPosition(-0.5,0.5,0.1);
        pLMissile->GetTransform()->SetSize(0.3,0.3,1);
        pLMissile->SetRenderable(new CSprite());
        pLMissile->SetMaterial(new CColourMaterial());
        pLMissile->GetMaterial()->SetTexture(PLAYER);
        PBase->AddChild(pLMissile);

		CGameObject * pRMissile=new CGameObject();
        pRMissile->GetTransform()->SetPosition(0.5,0.5,0.1);
        pRMissile->GetTransform()->SetSize(0.3,0.3,1);
        pRMissile->SetRenderable(new CSprite());
        pRMissile->SetMaterial(new CColourMaterial());
        pRMissile->GetMaterial()->SetTexture(PLAYER);
        PBase->AddChild(pRMissile);*/

}
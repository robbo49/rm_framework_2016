#pragma once
#include "irenderable.h"
class CCamera :
	public IRenderable
{
public:
	CCamera(void);
	~CCamera(void);
	bool Render();
	void AddedToGameObject();
	
	//static CVector3 s_vCameraPos;
};


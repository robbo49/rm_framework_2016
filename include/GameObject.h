#pragma once
#include "node.h"
#include <vector>

class IRenderable;
class IMaterial;
class IBehavior;
class CTransform;



class CGameObject :
	public CNode
{
public:
	CGameObject(void);
	~CGameObject(void);

	bool Render();
	void Update(float dt_);
	void RenderRecursive();
	void UpdateRecursive(float dt_);


	void SetRenderable(IRenderable * pRenderable_);
	void SetMaterial(IMaterial * pMaterial_);
	void AddBehavior(IBehavior * pBehavior_);
	void SetTransform(CTransform * pTransform_);

	IRenderable * GetRenderable();
	IMaterial * GetMaterial();
	//IBehavior * GetBehavior();
	CTransform * GetTransform();

private:

	IRenderable * m_pRenderable;
	IMaterial * m_pMaterial;
	//IBehavior * m_pBehavior;
	std::vector <IBehavior *> m_vBehaviors;
	CTransform * m_pTransform;

};
